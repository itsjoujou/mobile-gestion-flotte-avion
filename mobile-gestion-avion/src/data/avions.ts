import { Modele } from "./modeles";

export interface Avion {
    idAvion: number;
    annee: number;
    numeroPlaque: string;
    kilometrage: number;
    debutAssurance: Date;
    finAssurance: Date;
    modele: Modele;
    vitesseMax: number;
    photo: string;
    sieges: number;
    reacteurs: number;
    autonomie: number;
    envergure: number;
    hauteur: number;
    longueur: number;
}

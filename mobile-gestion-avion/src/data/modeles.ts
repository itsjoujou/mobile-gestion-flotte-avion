import { Constructeur } from "./constructeur";

export interface Modele {
    idModele: number;
    nom: string;
    constructeur: Constructeur;
}

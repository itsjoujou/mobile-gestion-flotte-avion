import {
    IonList
} from '@ionic/react';
import AvionListItem from './AvionListItem';
import { useParams } from 'react-router';
import { useEffect, useState } from 'react';
import { Avion } from '../data/avions';
import './AvionExpired.css';
  
const AvionExpired: React.FC = () => {
    const [avions, setAvions] = useState<Avion[]>([]);
    const params = useParams<{ monthDuration: string }>();

    const getAvionsWithAssuranceExpiredIn = async(monthDuration: number) => {
        try {
            const response = await fetch(`https://gestion-flotte-avion-production.up.railway.app/avions/expired/${monthDuration}`);
            const json = await response.json();
            setAvions(json.data);
            console.log(json.data);
        } catch (error) {
            console.error(error)
        }
    };

    useEffect(() => {
        getAvionsWithAssuranceExpiredIn( parseInt(params.monthDuration) );
    }, [params]);

    return (
        <IonList>
            { avions.map(avion => 
                <AvionListItem key={ avion.idAvion } avion={ avion } />
            )}
        </IonList>
    );
};
  
export default AvionExpired;
  
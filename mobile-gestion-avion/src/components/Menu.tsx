import {
  IonAccordion,
  IonAccordionGroup,
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuToggle,
  IonTitle,
  IonToolbar,
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { home, homeSharp, shieldCheckmark, shieldCheckmarkSharp } from 'ionicons/icons';
import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'Home',
    url: '/home',
    iosIcon: home,
    mdIcon: homeSharp
  }
];

const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="overlay">
      <IonToolbar color="light">
        <IonTitle>Flotte Avion</IonTitle>
      </IonToolbar>

      <IonContent >
        <IonList id="inbox-list">
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}

            <IonAccordionGroup>
              <IonAccordion value="first">
                <IonItem className={location.pathname.includes('/avions/expired/') ? 'accordion-expanded' : ''} slot="header">
                  <IonIcon slot="start" ios={shieldCheckmark} md={shieldCheckmarkSharp} />
                  <IonLabel>Expiration Assurance</IonLabel>
                </IonItem>
                <div className="ion-padding" slot="content">
                <IonMenuToggle key={1} autoHide={false}>
                    <IonItem className={location.pathname === '/avions/expired/1' ? 'selected' : ''} routerLink={'/avions/expired/1'} routerDirection="none" lines="none" detail={false}>
                      <IonLabel>Dans 1 mois</IonLabel>
                    </IonItem>
                </IonMenuToggle>
                <IonMenuToggle key={2} autoHide={false}>
                    <IonItem className={location.pathname === '/avions/expired/3' ? 'selected' : ''} routerLink={'/avions/expired/3'} routerDirection="none" lines="none" detail={false}>
                      <IonLabel>Dans 3 mois</IonLabel>
                    </IonItem>
                </IonMenuToggle>
                </div>
              </IonAccordion>
            </IonAccordionGroup>
        </IonList>

      </IonContent>
    </IonMenu>
  );
};

export default Menu;

import {
    IonList
} from '@ionic/react';
import AvionListItem from './AvionListItem';
import { useEffect, useState } from 'react';
import { Avion } from '../data/avions';
import './AvionList.css';
  
const AvionList: React.FC = () => {
    const [avions, setAvions] = useState<Avion[]>([]);

    const getAvions = async() => {
        try {
            const response = await fetch('https://gestion-flotte-avion-production.up.railway.app/avions');
            const json = await response.json();
            setAvions(json.data);
            console.log(json.data);
        } catch (error) {
            console.error(error)
        }
    };

    useEffect(() => {
        getAvions();
    }, []);

    return (
        <IonList>
            { avions.map(avion => 
                <AvionListItem key={ avion.idAvion } avion={ avion } />
            )}
        </IonList>
    );
};
  
export default AvionList;
  
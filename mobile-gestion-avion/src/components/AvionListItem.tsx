import {
    IonCard,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle} from '@ionic/react';
import { Avion } from '../data/avions';
import './AvionListItem.css';

interface AvionListItemProps {
    avion: Avion;
}
  
const AvionListItem: React.FC<AvionListItemProps> = ({ avion }) => {
    return (
        <IonCard routerLink={`/avions/${avion.idAvion}`} className="ion-padding">
            <IonCardHeader>
                <IonCardTitle>{avion.numeroPlaque}</IonCardTitle>
                <IonCardSubtitle>{avion.modele.constructeur.nom} {avion.modele.nom}</IonCardSubtitle>
            </IonCardHeader>
        </IonCard>
    );
};
  
export default AvionListItem;
  
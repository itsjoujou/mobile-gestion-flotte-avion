import {
    IonBackButton,
    IonButton,
    IonButtons,
    IonCard,
    IonCardContent,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonCol,
    IonContent,
    IonGrid,
    IonHeader,
    IonIcon,
    IonImg,
    IonItem,
    IonLabel,
    IonList,
    IonPage,
    IonRow,
    IonToolbar,
    useIonViewWillEnter
} from '@ionic/react';
import { create } from 'ionicons/icons';
import { Avion } from "../data/avions";
import { useState } from "react";
import { useHistory, useParams } from "react-router";
import './AvionDetail.css';

const AvionDetail: React.FC = () => {
    const [avion, setAvion] = useState<Avion>();
    const params = useParams<{ id: string }>();
    const history = useHistory();

    const getAvion = async(id: number) => {
        try {
            const response = await fetch(`https://gestion-flotte-avion-production.up.railway.app/avions/${id}`);
            const json = await response.json();
            setAvion(json.data);
            console.log(json.data);
        } catch (error) {
            console.error(error)
        }
    };

    useIonViewWillEnter(() => {
        const bearerToken = JSON.parse(localStorage.getItem("bearer-token")!);

        if (bearerToken === null || new Date().getTime() > new Date(bearerToken.expired_at).getTime()) {
            history.push('/login');
        }

        getAvion( parseInt(params.id) );
    }, []);

    return (
        <IonPage id="view-message-page">
            <IonHeader translucent>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton text={`${avion?.modele.constructeur.nom} ${avion?.modele.nom}`} color="primary" defaultHref="/home"></IonBackButton>
                    </IonButtons>
                    
                    <IonButtons slot="primary">
                        <IonButton fill="clear">
                            Edit
                            <IonIcon slot="end" icon={create}></IonIcon>
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonImg src={`data:image/jpeg;base64,/9j/${avion?.photo}`}></IonImg>

                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonCard color="default">
                                <IonCardHeader>
                                    <IonCardSubtitle>Année</IonCardSubtitle>
                                </IonCardHeader>

                                <IonCardContent>{avion?.annee}</IonCardContent>
                            </IonCard>
                        </IonCol>

                        <IonCol>
                            <IonCard color="default">
                                <IonCardHeader>
                                    <IonCardSubtitle>Immatriculation</IonCardSubtitle>
                                </IonCardHeader>

                                <IonCardContent>{avion?.numeroPlaque}</IonCardContent>
                            </IonCard>
                        </IonCol>

                        <IonCol>
                            <IonCard color="default">
                                <IonCardHeader>
                                    <IonCardSubtitle>Sièges</IonCardSubtitle>
                                </IonCardHeader>

                                <IonCardContent>{avion?.sieges}</IonCardContent>
                            </IonCard>
                        </IonCol>
                    </IonRow>
                </IonGrid>

                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>Spécifications</IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>

                        <IonList>
                            <IonItem>
                                <IonLabel>Constructeur</IonLabel>
                                <p className={'ion-text-right'}>{avion?.modele.constructeur.nom}</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Vitesse Maximale</IonLabel>
                                <p className={'ion-text-right'}>{avion?.vitesseMax} km/h</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Autonomie</IonLabel>
                                <p className={'ion-text-right'}>{avion?.autonomie} km</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Réacteurs</IonLabel>
                                <p className={'ion-text-right'}>{avion?.reacteurs} km</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Assurance expiré le</IonLabel>
                                <p className={'ion-text-right'}>{avion?.finAssurance.toString()}</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Kilométrage</IonLabel>
                                <p className={'ion-text-right'}>{avion?.kilometrage} km</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Longueur</IonLabel>
                                <p className={'ion-text-right'}>{avion?.longueur} m</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Envergure</IonLabel>
                                <p className={'ion-text-right'}>{avion?.envergure} m</p>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Hauteur</IonLabel>
                                <p className={'ion-text-right'}>{avion?.hauteur} m</p>
                            </IonItem>
                        </IonList>
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default AvionDetail;

import {
    IonBackButton,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonInput,
    IonItem,
    IonLabel,
    IonPage,
    IonToolbar
} from '@ionic/react';
import { useHistory } from 'react-router';
import React from 'react';
import { chevronBack } from 'ionicons/icons';

const Login: React.FC = () => {
    const username = "Miaa";
    const password = "root";

    const history = useHistory();
    const login = async(event: React.FormEvent) => {
        event.preventDefault();
        
        const request = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "username": username,
                "password": password
            })
        };

        fetch('https://gestion-flotte-avion-production.up.railway.app/users/authentificate', request)
            .then(response => response.json())
            .then(json => {
                console.log(json.data);
                if (json.data != null) {
                    sessionStorage.setItem("user", JSON.stringify(
                        {
                            idLogin: json.data.idLogin,
                            username: json.data.username
                        }
                    ));

                    localStorage.setItem("bearer-token", JSON.stringify(
                        {
                            token: json.data.token,
                            expired_at: json.data.expired_at
                        }
                    ));

                    history.push("/home");
                } else {
                    history.push("/login");
                }
            }
        );
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton color="primary" defaultHref="/home" icon={chevronBack}></IonBackButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <form className="ion-padding" onSubmit={login}>
                    <IonItem fill="outline">
                        <IonLabel position="floating">Username</IonLabel>
                        <IonInput value={username} type="text" name="username" />
                    </IonItem>
                    <IonItem fill="outline">
                        <IonLabel position="floating">Password</IonLabel>
                        <IonInput value={password} type="password" name="password" />
                    </IonItem>
                    <br />
                    <IonButton type="submit" className="ion-margin-top">Login</IonButton>
                </form>
            </IonContent>
        </IonPage>
    );
}

export default Login;
